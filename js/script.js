onload = () => {
  startGame()
}

const main = document.querySelector("main")
const footer = document.querySelector("footer")
let player = ""
let playerOption = 0
let computerOption = 0

let startGame = () =>{
  main.innerHTML = ""
  footer.innerHTML = ""
  let inputText = document.createElement("input")
  let button = document.createElement("button")
  
  main.classList.add("flexColumn")

  main.appendChild(inputText)
  inputText.placeholder = "Nome ou apelido"
  inputText.type = "text"

  
  main.appendChild(button)
  button.textContent = "GO!!"
  button.id = "goButton"
  
  let goButton = document.getElementById("goButton")

  let savePlayer = () =>{
    player = inputText.value
    goGame()  
  }
  goButton.addEventListener("click", savePlayer)

}

let goGame = () => {
  main.innerHTML = ""
  main.classList.remove("flexColumn")

  footer.appendChild(document.createElement("p")).textContent = `${player} escolha pedra papel ou tesoura`


  for(let i = 1; i <= 3; i++){
    main.insertAdjacentHTML("beforeend", `<img src="./images/${i}.svg" id="${i}">`)
    document.getElementById(i).addEventListener("click", saveOptions)
  }

  function saveOptions(e){
    playerOption = parseInt(e.srcElement.id)
    computerOption = Math.floor(Math.random() * (1 + 2)) + 1
    console.log(playerOption)
    console.log(computerOption)
    resultGame()

  }
} 


let resultGame = () => {
  let button = document.createElement("button")
  main.innerHTML = ""
  footer.innerHTML = ""

  footer.appendChild(button)
  button.textContent = "RESTART"
  button.id = "restartButton"

  if(playerOption === 1 && computerOption === 2){
    main.insertAdjacentHTML("beforeend", `<p>${player} você perdeu, seu adversário escolheu papel, tente novamente.</p>`)

  } else if(playerOption === 1 && computerOption === 3){
    main.insertAdjacentHTML("beforeend", `<p>Parabéns ${player} você ganhou, seu adversário escolheu tesoura.</p>`)
  } else if(playerOption === 2 && computerOption === 1){
    main.insertAdjacentHTML("beforeend", `<p>Parabéns ${player} você ganhou, seu adversário escolheu pedra.</p>`)
  } else if(playerOption === 2 && computerOption === 3){
    main.insertAdjacentHTML("beforeend", `<p>${player} você perdeu, seu adversário escolheu tesoura, tente novamente.</p>`)
  } else if(playerOption === 3 && computerOption === 1){
    main.insertAdjacentHTML("beforeend", `<p>${player} você perdeu, seu adversário escolheu pedra, tente novamente.</p>`)
  } else if(playerOption === 3 && computerOption === 2){
    main.insertAdjacentHTML("beforeend", `<p>Parabéns ${player} você ganhou, seu adversário escolheu papel.</p>`)
  } else{ 
    main.insertAdjacentHTML("beforeend", `<p>${player} empatou, tente novamente</p>`)
  }


  let restartButton = document.getElementById("restartButton")
  restartButton.addEventListener("click", startGame)

}



